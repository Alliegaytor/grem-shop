// Center Hero if width under 1200px and over 760px
if (window.matchMedia("(max-width: 1200px)").matches && window.matchMedia("(min-width: 760px)").matches) {
  document.addEventListener('DOMContentLoaded', (event) => {
      var marginLeftHero = (windowWidthHero - 560) /2;
      document.getElementById('hero').style.paddingLeft = `${marginLeftHero}px`;
  });
  var windowWidthHero = document.getElementById('hero').offsetWidth;
};

// Set youtube iframe height and width
if (window.matchMedia("(max-width: 560px").matches) {
  const youtube = document.getElementById('youtube');
  youtube.setAttribute('width', window.innerWidth + 'px');
  youtube.setAttribute('height', window.innerWidth / 16 * 9 + 'px');
};

// TOGGLE NAVBAR
function navToggle(collapse, hamburger) {
  collapse.classList.toggle('show');
  // https://stackoverflow.com/a/72543298
  hamburger.setAttribute(
    'aria-expanded', 
    `${!(hamburger.getAttribute('aria-expanded') === 'true')}`
  );
};

// SMOOTHSCROLL
function smoothScroll(elementid, event) {
  event.preventDefault(); // DISABLE LINK
  document.querySelector(elementid).scrollIntoView({
    behavior: `smooth`,
  });
};

// LOAD FUNCTIONS
window.addEventListener('load', (event) => {
  // CONSTANTS
  const navbar = document.getElementsByClassName('nav-link');
  const collapse = document.getElementById('navbarNav');
  const hamburger = document.getElementsByClassName('navbar-toggler')[0];

  // ADD EVENT LISTENERS
  for (let i = 0; i < navbar.length; i++) {
    let elementid = navbar[i].getAttribute('href');
    if (elementid.startsWith("#")) {
      navbar[i].addEventListener("click", function (event) {
        smoothScroll(elementid, event);
        navToggle(collapse, hamburger);
      });
    };
  };
  hamburger.addEventListener("click", function () {
    navToggle(collapse, hamburger);
  });
});


